# dbbrowser

## Name
dbbrowser

## Description
This app allow you to browse inside databases

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
You can see your local DISPLAY by opening a new terminal on your exceed session and running the next:

    echo $DISPLAY

if you opened a terminal, follow next steps:

    setenv path "$path:<something>/anaconda3/Linux/2019.03/bin"
    setenv DISPLAY "<USE YOUR DISPLAY HERE>"


Finally, just run the next command:

    dbbrowser.py


## Contributing
I'm opened to contributions and what your requirements are for accepting them.

## Authors and acknowledgment
Daniel Oberti <obertidaniel@gmail.com>

## License
GPL license


