#! /proj/inphitools00/cad/anaconda3/Linux/2019.03/bin/python
import tkinter  as tk 
from tkinter import * 
from tkinter import ttk, filedialog
from tkinter.filedialog import askopenfile
from sqlalchemy import create_engine
from sqlalchemy import inspect
import time

class Checkbox(ttk.Checkbutton):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.variable = tk.BooleanVar(self)
        self.configure(variable=self.variable)
    
    def checked(self):
        return self.variable.get()
    
    def check(self):
        self.variable.set(True)
    
    def uncheck(self):
        self.variable.set(False)

class SQLbrowser:

    def __init__(self):
        self.version = 'v0.1'
        print('*'*100)
        print('DBBROWSER {}'.format(self.version))
        print()
        print('     Contact: "doberti" (Write us for new features)')
        print()
        print('     Select a database...')
        print('*'*100)
        time.sleep(3)

        self.DEBUG = 0
        self.wind = tk.Tk()
        self.wind.geometry("1500x800")
        self.wind.title('DBBROWSER {}'.format(self.version))
        self.mytable = ''
        self.where = ''
        self.where2 = ''
        self.tupla_colnames = None
        self.db_name = self.open_file()
        self.conn = create_engine("sqlite:///{}".format(self.db_name))
        self.inspector = inspect(self.conn)

        #my_conn = create_engine("sqlite:///database.db")

        self.i=0 # row value inside the loop 

        # Create a Button
        #########################ttk.Button(self.wind, text="Browse", command = self.open_file).grid(row=self.i, column=0)
        self.i=self.i+2

        ttk.Button (self.wind, text='Open Table', command = self.opentable, width=100).grid(row = self.i, column = 0, columnspan = 8, padx=20, pady=0)
        self.i=self.i+4

        self.i_message = self.i
        self.message = Label(text='', fg='red')
        self.message.grid(row=self.i_message, column = 0, columnspan=8)
        self.i=self.i+2

        self.tree2 = ttk.Treeview (height=10, column=2)
        self.tree2.grid(row=self.i, column=0, columnspan=8, padx=20, pady=0)
        self.tree2.heading(0, text='Name', anchor = W)
        self.tree2.heading(2, text='Type', anchor = W)
        self.i=self.i+8

        self.i_label = self.i
        self.label = tk.Label(self.wind, text='Table selected: {}'.format(self.mytable), font=("Arial",20))
        #self.label.grid(row=self.i_label, column=0, columnspan=8)
        #self.label.grid_remove()


        ######### OPCIONES FILTR0 GENERAL ##############
        self.i=self.i+4
        self.i_filter = self.i

        self.combo_titles = ttk.Combobox(self.wind, width=27)
        self.combo_operation = ttk.Combobox(self.wind,width=10)
        self.combo_operation['values']= ('','=','!=','>','<','>=','<=','LIKE', 'IS NULL')
        self.combo_operation.current(0) #set the selected item
        self.entry_data = ttk.Entry(self.wind,width=30)
        self.btn_filter = ttk.Button (self.wind, text = 'Filter', command = self.filter, width=15)
        self.btn_reset = ttk.Button (self.wind, text = 'Reset', command = self.reset, width=15)

        ######### OPCIONES FILTR0 FECHA ##############
        self.i = self.i+2
        self.i_checkbox_filterdate=self.i
        self.checkbox_filterdate = Checkbox(self.wind, text="Filter by a Date field", command=self.check_filterdate)

        self.i = self.i+2
        self.i_filter_date = self.i
        self.combo_titles2 = ttk.Combobox(self.wind, width=27)
        self.combo_operation2 = ttk.Combobox(self.wind,width=13)
        self.combo_operation2['values']= ('between')
        self.combo_operation2.current(0) #set the selected item
        self.entry_filterdate1 = ttk.Entry(self.wind,width=25)
        self.label_and = Label(text=' AND ', width=4)
        self.entry_filterdate2 = ttk.Entry(self.wind,width=25)
        self.btn_filter2 = ttk.Button (self.wind, text = 'Filter', command = self.filter2, width=15)
        self.btn_reset2 = ttk.Button (self.wind, text = 'Reset', command = self.reset2, width=15)

        ######### LISTBOX ##############
        self.i=self.i+2
        self.i_listBox = self.i
        self.listBox = None
        self.scrolltable = None

        self.viewing_tables()

        self.wind.mainloop()

    def check_filterdate(self):
        if self.checkbox_filterdate.checked():
            self.combo_titles2.grid(column=0, row=self.i_filter_date, columnspan=1, padx=5, pady=5)
            self.combo_operation2.grid(column=1, row=self.i_filter_date, padx=5, pady=5)
            self.entry_filterdate1.grid(column=2, row=self.i_filter_date, columnspan=1, padx=0, pady=5)
            self.label_and.grid(column=2, row=self.i_filter_date, columnspan=1, padx=4, pady=5)
            self.entry_filterdate2.grid(column=3, row=self.i_filter_date, columnspan=1, padx=3, pady=5)
            self.btn_filter2.grid(column=4, row=self.i_filter_date, columnspan=1, padx=0, pady=5)
            self.btn_reset2.grid(column=5, row=self.i_filter_date, columnspan=1, padx=0, pady=5)
        else:
            self.combo_titles2.grid_remove()
            self.combo_operation2.grid_remove()
            self.entry_filterdate1.grid_remove()
            self.label_and.grid_remove()
            self.entry_filterdate2.grid_remove()
            self.btn_filter2.grid_remove()
            self.btn_reset2.grid_remove()

    def run_query(self, query, parameters = ()):
        query_result = self.conn.execute (query, parameters)
        return query_result

    def viewing_tables (self):
        records = self.tree2.get_children()
        for element in records:
            self.tree2.delete(element)
        #query = "SELECT name FROM sqlite_master WHERE type='table';"
        #db_rows = self.run_query(query)
        #for row in db_rows:
        #    self.tree2.insert('',0,text = row[0], values = 'table')
        
        # Get table information
        if self.DEBUG: print(self.inspector.get_table_names())
        for table_name in self.inspector.get_table_names():
            if table_name != 'sqlite_sequence':
                self.tree2.insert('',0,text = table_name, values = 'table')

    def reset (self):
        self.combo_operation.current(0)
        self.entry_data.delete(0, END)
        self.entry_data.update()
        self.where = ''
        self.viewing_records2()

    def filter (self, where=''):
        self.where = ''
        title = self.combo_titles.get()
        operation = self.combo_operation.get()
        data = self.entry_data.get()

        if operation == 'IS NULL':
            self.where = ' AND {} {} '.format(title, operation)
        elif operation != '':
            self.where = ' AND {} {} "{}" '.format(title, operation, data)
        self.viewing_records2 ()

    def reset2 (self):
        self.combo_operation2.current(0)
        self.entry_filterdate1.delete(0, END)
        self.entry_filterdate2.delete(0, END)
        self.where2 = ''
        self.viewing_records2()

    def filter2 (self, where=''):
        self.where2 = ''
        title = self.combo_titles2.get()
        operation = self.combo_operation2.get()
        date1 = self.entry_filterdate1.get()
        date2 = self.entry_filterdate2.get()

        # AGREGAR CONTROL PARA VERIFICAR SI EL CAMPO seleccionado ('title') ES DE TIPO DATE REALMENTE!

        if operation!='' and date1!='' and date2!='':
            self.where2 = ' AND {} {} "{}" and "{}" '.format(title, operation, date1, date2)
        else:
            print('Error on filter2')
        self.viewing_records2 ()

    def viewing_records2(self):
        #records = self.listBox.get_children()
        #for element in records:
        #    self.listBox.delete(element)
        #self.listBox.grid(row=self.i_listBox, column=0, columnspan=7)

        if(self.listBox): self.listBox.destroy()
        if(self.scrolltable): self.scrolltable.destroy()
        
        query = 'SELECT * FROM {} WHERE 1=1 {} {} ORDER BY 1 ASC LIMIT 0,100000'.format(self.mytable, self.where, self.where2)
        if self.DEBUG: print( query )
        query_result = self.conn.execute (query)


        ###########################self.label.config(text=self.mytable)
        
        # create Treeview
        cols = self.tupla_colnames

        self.listBox = ttk.Treeview(self.wind, columns=cols, show='headings')

        # set column headings
        for col in cols:
            self.listBox.heading(col, text=col, anchor = W)   
        
        for myrow in query_result:
            if self.DEBUG: print(myrow)
            self.listBox.insert("", tk.END, values=tuple(myrow))

        self.scrolltable = ttk.Scrollbar(self.wind, orient="vertical", command=self.listBox.yview)
        self.listBox.configure(yscrollcommand=self.scrolltable.set)

        self.listBox.grid(row=self.i_listBox, column=0, columnspan=8, padx=20, pady=1)
        self.scrolltable.grid(row=self.i_listBox, column=8, sticky='nse')

        self.wind.mainloop()

    def open_file(self):
        file = filedialog.askopenfile(mode='r', filetypes=[('Sqlite db file', '*.db')])
        if self.DEBUG: print(file.name)
        if file:
            return file.name
        self.conn = create_engine("sqlite:///{}".format(self.db_name))
        self.inspector = inspect(self.conn)

        self.wind.mainloop()


    def opentable (self):
        self.message['text'] = ""
        try:
            self.tree2.item(self.tree2.selection())['text']
        except IndexError as e:
            self.message['text'] = 'Please, select a table!'
            return
        name = self.tree2.item(self.tree2.selection())['text']
        if name != '':
            self.message['text'] = '' #"Table '{}' selected".format(name)
            self.mytable = name

            self.label.grid(row=self.i_label, column=0, columnspan=8)

            self.combo_titles.grid(column=0, row=self.i_filter, columnspan=1, padx=5, pady=5)
            self.combo_operation.grid(column=1, row=self.i_filter, columnspan=1, padx=0, pady=5)
            self.entry_data.grid(column=2, row=self.i_filter, columnspan=1, padx=0, pady=5)
            self.btn_filter.grid(row=self.i_filter, column=3, columnspan=1, padx=0, pady=5)
            self.btn_reset.grid(row=self.i_filter, column=4, columnspan=1, padx=0, pady=5)

            self.checkbox_filterdate.grid(row=self.i_checkbox_filterdate, column = 0, padx=10, pady=5)

            colnames = self.inspector.get_columns(self.mytable)
            if self.DEBUG: print(colnames)

            lista_colnames = []
            for col in colnames:
                lista_colnames.append(col['name'])

            self.tupla_colnames = tuple(lista_colnames)
            
            self.combo_titles['values']= self.tupla_colnames
            self.combo_titles.current(0) #set the selected item

            self.combo_titles2['values']= self.tupla_colnames
            self.combo_titles2.current(0) #set the selected item

            self.label['text'] = text='Table: {}'.format(self.mytable)

            self.viewing_records2()
        else:
            self.message['text'] = 'Please, select a table first!'




if __name__ == '__main__':
  application = SQLbrowser()

